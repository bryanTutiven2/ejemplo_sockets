#include "csapp.h"

void echo(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	//size_t n;
	char buf[MAXLINE];
	char buffer[MAXLINE];
	rio_t rio;
	int fd,a;
	Rio_readinitb(&rio, connfd);
	while(recv(connfd, buf, MAXLINE, 0) > 0 ) {
		
		for (int i = 0; i < strlen(buf); i++)
		{
			if(i == strlen(buf)-1){
				//permit reeemplazar el \n por \0 que es el fin de linea para que se pueda leer el archivo
				buf[i]= '\0';
			}
		}
		
		printf("%s", buf);
		a = strlen(buf);
		printf("server received %d bytes\n", a);
		printf("%s", buf);
		//Rio_writen(connfd, buf, n);
		
		
		fd = open(buf,O_RDONLY);
		if(fd>0){
			rio_readn(fd,buffer,MAXLINE);
			send(connfd, buffer, MAXLINE,0);
			memset( buffer, 0, sizeof( buffer) );
			memset( buf, 0, sizeof( buf) );
		}
		else{
			//En caso de no encontrar un archivo
			send(connfd, "0", 2,0);
			//memset( buffer, 0, sizeof( buffer) );
			memset( buf, 0, sizeof( buf) );
		}
		//rio_readn(fd,buffer,MAXLINE);
		//printf("%s\n", buffer);

		//send(connfd, buffer, MAXLINE,0);
		//memset( buffer, 0, sizeof( buffer) );
		//memset( buf, 0, sizeof( buf) );
		
	}
	
}
