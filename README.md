# README #
#Integrantes 
Bryan Tutiven
Juan Jimenez

//Indicaciones//
-Matar los procesos con kill -9 idDelProceso

-Para ver el id usar ps aux en el terminal

-Para salir del cliente usar ctrl z pero los procesos seguiran corriendo en segundo plano

//Fin Indicaciones//

Ejemplos que usan la interface sockets en Linux, basados en los ejemplos del cap?ulo de libro [Computer Systems: A Programmer's Perspective, 3/E](http://csapp.cs.cmu.edu/3e/home.html)

### Compilaci? ###

* Compilar el cliente: 
	* make client

* Compilar el servidor:
	* make server

* Compilar todo:
	* make
	
### Uso ###
Ejecutar el cliente:

```
./client <host> <port>
```
Ejemplo:

```
./client 127.0.0.1 8080
```

Ejecutar el cliente:

```
./server <port>
```
Ejemplo:

```
./server 8080
```