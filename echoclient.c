#include "csapp.h"
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

int main(int argc, char **argv)
{

	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	char buffer[MAXLINE];
	rio_t rio;
	int cont = 1;
	if (argc != 3) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];

	clientfd = Open_clientfd(host, port);
	printf("Escribir nombre del archivo: \n");

	Rio_readinitb(&rio, clientfd);
	while (Fgets(buf, MAXLINE, stdin) != NULL) {
		
		//Rio_writen(clientfd, buf, strlen(buf));
		//Rio_readlineb(&rio, buf, MAXLINE);
		//Fputs(buf, stdout);
		
	  	send(clientfd, buf, MAXLINE, 0); //envio
	  	recv(clientfd, buffer, MAXLINE, 0); //
	  	Fputs(buffer, stdout);
	  	if (strcmp(buffer, "0") == 0)
	  	{
	  		fprintf(stderr, "Archivo requerido no existe\n");
	  	}
	  	else{
	  		char ca[2];
	  		char direccion[] = "./archivoN/";
	  		ca[0]  = '0' + cont;
	  		strcat(direccion,ca);
	  		strcat(direccion,"Archivo.txt");
		  	FILE *fp;
		 	fp = fopen (direccion, "w+" );
		 	fwrite( buffer, sizeof(char), sizeof(buffer), fp );
		 	fclose ( fp );
		 	cont++;
	  	}
	  	memset( buffer, 0, sizeof( buffer) );
	  	
	}
	Close(clientfd);
	exit(0);
}
